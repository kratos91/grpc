package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/kratos91/grpc/products/productpb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var collection *mongo.Collection

type product struct {
	ID    primitive.ObjectID `bson:"_id,omitempty"`
	Name  string             `bson:"name"`
	Price float64            `bson:"price"`
}

type server struct {
	productpb.UnimplementedProductServiceServer
}

func (s *server) CreateProduct(ctx context.Context, req *productpb.CreateProductRequest) (*productpb.CreateProductResponse, error) {
	// parse content and save to mongo
	prod := req.GetProduct()
	data := product{
		Name:  prod.GetName(),
		Price: prod.GetPrice(),
	}

	res, err := collection.InsertOne(context.Background(), data)

	if err != nil {
		return nil, fmt.Errorf("code:%v", codes.Internal, " error:%s", err.Error())
	}

	oid, ok := res.InsertedID.(primitive.ObjectID)

	if !ok {
		return nil, fmt.Errorf("code:%v", codes.Internal, " cannot convert ObjectID")
	}

	return &productpb.CreateProductResponse{
		Product: &productpb.Product{
			Id:    oid.Hex(),
			Name:  prod.GetName(),
			Price: prod.GetPrice(),
		},
	}, nil
}

func (s *server) GetProduct(ctx context.Context, req *productpb.GetProductRequest) (*productpb.GetProductResponse, error) {
	productId := req.GetProductId()
	oid, err := primitive.ObjectIDFromHex(productId)

	if err != nil {
		return nil, fmt.Errorf("cannot parse ID->%v", err)
	}

	// create empty struct
	data := &product{}

	filter := bson.M{"_id": oid}

	res := collection.FindOne(context.Background(), filter)

	if err := res.Decode(data); err != nil {
		return nil, fmt.Errorf("cannot find product->%v", err)
	}

	return &productpb.GetProductResponse{
		Product: dbToProductPb(data),
	}, nil
}

func (s *server) ListProducts(req *productpb.ListProductRequest, stream productpb.ProductService_ListProductsServer) error {
	fmt.Println("Listing products server side")

	cur, err := collection.Find(context.Background(), primitive.D{{}})
	if err != nil {
		return fmt.Errorf("error getting products")
	}

	defer cur.Close(context.Background())

	for cur.Next(context.Background()) {
		data := &product{}
		err := cur.Decode(data)

		if err != nil {
			return fmt.Errorf("error decoding product->%v", err)
		}

		stream.Send(&productpb.ListProductResponse{
			Product: dbToProductPb(data),
		})
	}
	if err := cur.Err(); err != nil {
		return fmt.Errorf("error decodign product->%v", err)
	}
	return nil
}

func (s *server) UpdateProduct(ctx context.Context, req *productpb.UpdateProductRequest) (*productpb.UpdateProductResponse, error) {
	fmt.Println("Update product request")

	prod := req.GetProduct()

	oid, err := primitive.ObjectIDFromHex(prod.GetId())

	if err != nil {
		return nil, fmt.Errorf("cannot parse ID->%v", err)
	}

	// Create empty struct
	data := &product{}
	filter := bson.M{"_id": oid}

	// search the product in db
	res := collection.FindOne(context.Background(), filter)
	if err = res.Decode(data); err != nil {
		return nil, fmt.Errorf("error decoding id->%v", err)
	}

	// update the internal struct product
	data.Name = prod.GetName()
	data.Price = prod.GetPrice()

	_, err = collection.ReplaceOne(context.Background(), filter, data)
	if err != nil {
		return nil, fmt.Errorf("error updating-> %v", err)
	}
	return &productpb.UpdateProductResponse{
		Product: dbToProductPb(data),
	}, nil
}

func (s *server) DeleteProduct(ctx context.Context, req *productpb.DeleteProductRequest) (*productpb.DeleteProductResponse, error) {
	fmt.Println("Delete product request")

	oid, err := primitive.ObjectIDFromHex(req.GetProductId())

	if err != nil {
		return nil, fmt.Errorf("cannot parse ID->%v", err)
	}

	filter := bson.M{"_id": oid}

	res, err := collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return nil, fmt.Errorf("error deleting product->%v", err)
	}

	if res.DeletedCount == 0 {
		return nil, fmt.Errorf("Product not found %s", req.GetProductId())
	}

	return &productpb.DeleteProductResponse{
		ProductId: req.GetProductId(),
	}, nil
}

func dbToProductPb(data *product) *productpb.Product {
	return &productpb.Product{
		Id:    data.ID.Hex(),
		Name:  data.Name,
		Price: data.Price,
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))

	if err != nil {
		log.Fatalf("Error conection with mongodb->%v", err)
	}

	collection = client.Database("productsdb").Collection("products")

	fmt.Println("Products, Go server is running")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")

	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

	s := grpc.NewServer()

	productpb.RegisterProductServiceServer(s, &server{})

	go func() {
		fmt.Println("Starting server...")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Error-> %v", err)
		}
	}()

	// Wait for ctrl+x to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until we get the signal
	<-ch
	fmt.Println("Stopping the server...")
	s.Stop()
	fmt.Println("Closing the listener")
	lis.Close()
	client.Disconnect(context.TODO())
	fmt.Println("Goodbye boy...")
}
