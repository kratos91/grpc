package main

import (
	"context"
	"fmt"
	"io"
	"log"

	"bitbucket.org/kratos91/grpc/products/productpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Go client is running")

	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())

	if err != nil {
		log.Fatalf("Failed to connect %v", err)
	}

	defer cc.Close()

	c := productpb.NewProductServiceClient(cc)

	// Creating product
	product := &productpb.Product{
		Name:  "Smarthphone Huawei",
		Price: 20500.5,
	}
	createProduct, err := c.CreateProduct(context.Background(), &productpb.CreateProductRequest{
		Product: product,
	})

	if err != nil {
		log.Fatalf("Failed to create product %v", err)
	}

	fmt.Printf("Product %v created", createProduct)

	// Getting product
	productID := createProduct.GetProduct().GetId()

	getProductReq := &productpb.GetProductRequest{ProductId: productID}

	getProductRes, getProductErr := c.GetProduct(context.Background(), getProductReq)

	if getProductErr != nil {
		log.Fatalf("Failed to getiing product %v", getProductErr)
	}

	fmt.Printf("Product got ->%v", getProductRes)

	// Updating product

	newProduct := &productpb.Product{
		Id:    productID,
		Name:  "Iphone 13",
		Price: 25000.70,
	}

	updateRes, updateErr := c.UpdateProduct(context.Background(), &productpb.UpdateProductRequest{
		Product: newProduct,
	})

	if updateErr != nil {
		fmt.Errorf("error happening while updating: %v", updateErr)
	}

	fmt.Printf("the product was updated: %v", updateRes)

	// Deleting product
	deleteRes, deleteErr := c.DeleteProduct(context.Background(), &productpb.DeleteProductRequest{
		ProductId: productID,
	})

	if deleteErr != nil {
		fmt.Errorf("Error deleting product->%v", deleteErr)
	}

	fmt.Printf("Product deleted %v \n", deleteRes)

	// List products
	stream, err := c.ListProducts(context.Background(), &productpb.ListProductRequest{})

	if err != nil {
		log.Fatalf("Error listing products->%v", err)
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving->%v", err)
		}

		fmt.Println(res.GetProduct())
	}
}
