package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"bitbucket.org/kratos91/grpc/hello/hellopb"
	"google.golang.org/grpc"
)

func helloUnary(c hellopb.HelloServiceClient) {
	fmt.Println("Starting unary RPC Hello")

	req := &hellopb.HelloRequest{
		Hello: &hellopb.Hello{
			FirstName: "Emiliano",
			Prefix:    "Joven",
		},
	}

	res, err := c.Hello(context.Background(), req)

	if err != nil {
		log.Fatalf("Error,calling Hello RPC: %v\n", err)
	}
	log.Printf("Response Hello: %v", res.CustomHello)
}

func helloServerStreaming(c hellopb.HelloServiceClient) {
	fmt.Println("Starting server streaming RPC Hello")

	req := &hellopb.HelloManyLanguagesRequest{
		Hello: &hellopb.Hello{
			FirstName: "Emiliano",
			Prefix:    "Joven",
		},
	}

	restStream, err := c.HelloManyLanguages(context.Background(), req)

	if err != nil {
		log.Fatalf("Error in stream: %v", err)
	}

	for {
		msg, err := restStream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Erro reading stream %v", err)
		}
		log.Printf("Response-> %v\n", msg.GetHelloLenguage())
	}
}

func goodByeClientStreaming(c hellopb.HelloServiceClient) {
	fmt.Println("Starting client streaming RPC Hello")

	requests := []*hellopb.HelloGoodbyeRequests{
		{
			Hello: &hellopb.Hello{FirstName: "Emiliano", Prefix: "Joven"},
		},
		{
			Hello: &hellopb.Hello{FirstName: "Ruben", Prefix: "Joven"},
		},
		{
			Hello: &hellopb.Hello{FirstName: "Jose", Prefix: "Sr"},
		},
	}

	stream, err := c.HellosGoodbye(context.Background())

	if err != nil {
		log.Fatalf("Error calling goodbye %v", err)
	}

	for _, req := range requests {
		fmt.Printf("Sending request: %v", req)

		stream.Send(req)
		time.Sleep(time.Second)
	}
	goodbye, err := stream.CloseAndRecv()

	if err != nil {
		log.Fatalf("Error goodbye recived->%v", err)
	}
	fmt.Println("Response->", goodbye)
}

func goodByeBidiStreaming(c hellopb.HelloServiceClient) {
	fmt.Println("Starting Bidirectional streaming RPC Hello")

	// create stream to call server
	stream, err := c.Goodbye(context.Background())

	requests := []*hellopb.GoodByeRequest{
		{
			Hello: &hellopb.Hello{FirstName: "Emiliano", Prefix: "Joven"},
		},
		{
			Hello: &hellopb.Hello{FirstName: "Ruben", Prefix: "Joven"},
		},
		{
			Hello: &hellopb.Hello{FirstName: "Jose", Prefix: "Sr"},
		},
	}

	if err != nil {
		log.Fatalf("Error creating stream %v", err)
	}

	waitC := make(chan struct{})
	// send many messages to the server
	go func() {
		for _, req := range requests {
			log.Printf("Sending message :%v", req)
			stream.Send(req)
			time.Sleep(time.Second)
		}
		stream.CloseSend()
	}()

	// receiving messages from the server
	go func() {
		for {
			res, err := stream.Recv()

			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error receiving stream: %v", err)
				break
			}
			fmt.Printf("Got it: %v\n", res.GetGoodBye())
		}

		close(waitC)
	}()
	// block when everything is completed or closed
	<-waitC
}

func main() {
	fmt.Println("Go client is running")

	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())

	if err != nil {
		log.Fatalf("Failed to connect %v", err)
	}

	defer cc.Close()

	c := hellopb.NewHelloServiceClient(cc)

	// Unary
	//helloUnary(c)

	//Server Streaming
	//helloServerStreaming(c)

	//Client Streaming
	//goodByeClientStreaming(c)

	//Bidirectional Streaming
	goodByeBidiStreaming(c)
}
