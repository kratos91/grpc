package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"bitbucket.org/kratos91/grpc/hello/hellopb"
	"google.golang.org/grpc"
)

type server struct {
	hellopb.UnimplementedHelloServiceServer
}

func (s *server) Hello(ctx context.Context, req *hellopb.HelloRequest) (*hellopb.HelloResponse, error) {
	fmt.Printf("Hello function was called with %v", req)
	firstName := req.GetHello().GetFirstName()
	prefix := req.GetHello().GetPrefix()

	customeHello := "Welcome " + prefix + " " + firstName

	res := &hellopb.HelloResponse{
		CustomHello: customeHello,
	}
	return res, nil
}

func (s *server) HelloManyLanguages(req *hellopb.HelloManyLanguagesRequest, stream hellopb.HelloService_HelloManyLanguagesServer) error {
	fmt.Printf("Hello many times function was invoke with %v\n", req)

	langs := [7]string{"Salut! ", "Hello! ", "Ni hao! ", "Alô! ", "Privyét! ", "Schalom! ", "Hola! "}

	firstName := req.GetHello().GetFirstName()
	prefix := req.GetHello().GetPrefix()

	for _, helloLang := range langs {
		helloLanguage := helloLang + " " + prefix + " " + firstName

		res := &hellopb.HelloManyLanguagesResponses{
			HelloLenguage: helloLanguage,
		}

		stream.Send(res)
		time.Sleep(1 * time.Second)
	}

	return nil
}

func (s *server) HellosGoodbye(stream hellopb.HelloService_HellosGoodbyeServer) error {
	fmt.Printf("Goodbye function was invoked\n")
	goodbye := "Goodbye guys: "

	for {
		req, err := stream.Recv()

		if err == io.EOF {
			// Once is finished the stream we gonna send the response
			return stream.SendAndClose(&hellopb.HelloGoodbyeResponse{
				GoodBye: goodbye,
			})
		}
		if err != nil {
			log.Fatalf("Error in goodbye function-> %v", err)
		}

		firstName := req.GetHello().GetFirstName()
		prefix := req.GetHello().GetPrefix()

		goodbye += prefix + " " + firstName + " "
	}
}

func (s *server) Goodbye(stream hellopb.HelloService_GoodbyeServer) error {
	fmt.Printf("Goodbye bidirectional function was invoked\n")

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error reading the client stream: %v", err)
			return err
		}

		firstName := req.GetHello().GetFirstName()
		prefix := req.GetHello().GetPrefix()

		goodbye := "GoodBye " + prefix + " " + firstName

		sendErr := stream.Send(&hellopb.GoodByeResponse{
			GoodBye: goodbye,
		})

		if sendErr != nil {
			log.Fatalf("Error sending to the client: %v", err)
		}
	}
}

func main() {
	fmt.Println("Hello, Go server is running")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")

	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

	s := grpc.NewServer()

	hellopb.RegisterHelloServiceServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error-> %v", err)
	}

}
