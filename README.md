# Arquitecturas basicas de GRPC

### Unary

- Ejemplo de Unary en las carpetas de Hello funcion Hello (client.go)
- Revisar funcion Hello (server.go)
- Usermgmt

### Server Streaming
- Revisar funcion HelloManyLanguages en Hello (client.go)
- Revisar funcion HelloManyLanguages (server.go)

### Client Streaming
- Revisar funcion goodByeClientStreaming en Hello (client.go)
- Revisar funcion HellosGoodbye (server.go)

### Bidirectional Streaming
- Revisar funcion Goodbye en Hello (client.go)
- Revisar funcion goodByeBidiStreaming (server.go)

### CRUD simple with grpc and mongodb

#### Comando para generar el protobuffer con grpc

```sh
$ > protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative usermgmt/usermgmt.proto
```

#### Plugins necesarios para generar el protobuffer
```sh
$ > go install google.golang.org/protobuf/cmd/protoc-gen-go
```

```sh
$ > go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
```

##### Links importantes

- [Documentación GRPC](https://grpc.io/docs/languages/go/quickstart/)

- [Ejemplo básico para generar los protobuffers](https://www.youtube.com/watch?v=YudT0nHvkkE&list=PLrSqqHFS8XPYu-elDr1rjbfk0LMZkAA4X)